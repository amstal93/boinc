#!/bin/bash

# Find out where we're running from
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! "$(pwd)" == "${script_dir}" ]; then
  echo "Not allowed. First execute:"
  echo "cd ${script_dir}"
  echo " Then try again."
  exit 1
fi

options=()

# shellcheck disable=SC1090
reposettings="${script_dir}/repo_config.txt"
# shellcheck disable=SC1090
source "${reposettings}"

# read parameters from local .config file
# shellcheck disable=SC2154
usersettings="${HOME}/.config/docker/${container}-config.txt"
if [ -f "${usersettings}" ]; then
  # shellcheck disable=SC1090
  source "${usersettings}"
else
  echo "No custom settings file (${usersettings}) found. Using defaults."
fi

# double quoting (SC2086) will cause errors with docker run. Therefore:
echo "Starting the container with these options:"
# shellcheck disable=SC2086
echo ${options[*]}
echo
# double quoting (SC2086) will cause errors with docker run. Therefore:
# shellcheck disable=SC2086 disable=SC2154
docker run ${options[*]} "${image}" || exit 1

# shellcheck disable=SC2154
if [ ! -f "${flag}" ]; then
  echo "Initialising BOINC for the first time..."
  docker exec "${container}" boinccmd --join_acct_mgr "${MGR}" "${USR}" "${PSK}" || exit 1
  mkdir -p "$(dirname "${flag}")"
  touch "${flag}"
fi

# Always run with the latest updates.
docker exec -it "${container}" apt-get update
docker exec -it "${container}" apt-get -y upgrade

sleep 10

docker exec -it "${container}" boinctui

docker logs -f "${container}"
